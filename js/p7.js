/**
 * Text Templates overview
 * 
 * @module ig
 */
const tt = {};

/**
 * Initializes p7
 * 
 * @function init
 * 
 * @param {Object} lang - apex.lang
 * @param {Object} message - apex.message
 * @param {Object} region - apex.region
 * @param {Object} debug - apex.debug
 * @param {Object} ajaxUtil AJAX utilities
 */
const init = (lang, message, region, debug, ajaxUtil) => {
  debug.log("p7.init");

  tt.action = {
    igId: "ig_tt",
    actionId: "compile_tt",
    actionLabelKey: "UTL_COMPILE_TT",
    shortcut: "Alt+C"
  };

  /**
   * Compile action execution.
   */
  tt.compile_tt = (igId) => {
    ajaxUtil.execIg(igId, 'TT_ID', 'COMPILE_TT1', 'UTL_TT_COMPILE_DONE');
  };

  return tt;
};

export default {
  init
};
