/**
 * Text Templates overview
 * 
 * @module dvw
 */
const dvw = {};

/**
 * Initializes p7
 * 
 * @param {Object} lang - apex.lang
 * @param {Object} message - apex.message
 * @param {Object} region - apex.region
 * @param {Object} debug - apex.debug
 * @param {Object} ajaxUtil AJAX utilities
*/
const init = (lang, message, region, debug, ajaxUtil) => {
  debug.log("p13.init");

  dvw.action = {
    igId: "ig_dvw",
    actionId: "validate_dvw",
    actionLabelKey: "UTL_VALIDATE_DVW",
    shortcut: "Alt+V"
  };

  /**
   * Validate action execution.
   */
  dvw.validate_dvw = (igId) => {
    ajaxUtil.execIg(igId, 'DVW_ID', 'VALIDATE_DVW', 'UTL_DVW_VALIDATE_DONE');
  };

  return dvw;
};

export default {
  init
};
