/**
 * Miscellaneous utility Javascript functions.
 *
 * @module util
 */

const util = {};

/**
 * Initializes the utilities
 * @function init
 * 
 * @param {Object} debug   APEX Debug module
 * @param {Object} message APEX message module
 * @param {Object} lang    APEX lang module
 * 
 * @returns {Object} IG utilities module
 * 
 * @example
 * util = util.init(apex.debug, apex.message);
 **/
const init = (debug, message, lang) => {
   debug.log("util.init");

   /**
    * Gives version info.
        * @function info
    */
   util.info = () => {
      debug.info('util v1.0.0');
   };

   /**
    * Shows the UTL_FATAL_ERROR message.
    * 
    * @function fatalError
    */
   util.fatalError = () => {
      message.clearErrors();
      message.showErrors(
         {
            type: "error",
            location: "page",
            message: lang.getMessage("UTL_FATAL_ERROR")
         });
   };

   return util;
};

export default {
   init
}
