/**
 * Text Templates overview
 * 
 * @module igAction
 */
const igAction = {};
/**
 * 
 * @param {object} $ - apex.jQuery 
 * @param {object} lang - apex.lang
 * @param {object} region  - apex.region
 * @param {object} debug - apex.debug
 * @param {object} action - action object {igId, actionId, actionLabelKey, shortcut}
 */
const init = ($, lang, region, debug, action) => {
   debug.log("igAction.init");
   /**
    * Static ID IG
    * 
    * @constant 
    * @type {string}
    * @default
    */
   const igId = action.igId;
   /**
    * jQuery element ttIG
    * 
    * @constant
    * @type {object}
    * @default
    */
   const igId$ = $("#" + igId);


   /**
    * Initializes IG.
    * 
    * @param {object} config - IG config 
    */
   igAction.initIG = (config) => {
      var toolbarData = $.apex.interactiveGrid.copyDefaultToolbar();
      var toolbarGroup = toolbarData.toolbarFind("actions3");

      //Button
      toolbarGroup.controls.unshift({
         type: "BUTTON",
         label: lang.getMessage(action.actionLabelKey),
         action: action.actionId,
         id: action.actionId + "$button"
      });
      config.toolbarData = toolbarData;

      //Action
      config.initActions = (actions) => {
         actions.add([
            {
               name: action.actionId,
               labelKey: action.actionLabelKey,
               shortcut: action.shortcut,
               action: () => {
                  apex.event.trigger(igId$, action.actionId + "$event");
               }
            }
         ]);

         //Only enabled when there are records selected
         $(actions.context).on("interactivegridselectionchange", (event, data) => {
            var method = "enable";
            if (data.selectedRecords.length === 0) {
               method = "disable";
            }

            actions[method](action.actionId);
         })
      };
      return config;
   };

   /**
    * Initializes IG selecetion menu
    */
   igId$.on("interactivegridcreate", () => {
      var view = region(igId).call("getViews", "grid");
      var items;
      var menu$ = view.selActionMenu$;
      items = menu$.menu("option").items;
      items.push({
         type: "separator"
      });
      items.push({
         id: action.actionId,
         type: "action",
         action: action.actionId
      });
   });

   /**
    * Disables the action provided.
    * 
    * @param {Object} action Action to handle
    */
   igAction.disable = (action) => {
      region(action.igId).call("getActions")["disable"](action.actionId);
   };

   /**
    * Enables the action provided.
    * 
    * @param {Object} action Action to handle
    */
   igAction.enable = (action) => {
      region(action.igId).call("getActions")["enable"](action.actionId);
   };


   return igAction;
};

export default {
   init
};
