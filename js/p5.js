/**
 * Initializes p5 module
 * @function init
 * 
 * @param {string} debug APEX debug module
 * 
 * @example
 * p5.init();
 **/
const init = (debug) => {
    debug.log("p5.init");
};
   
export default {
  init
};
 
 