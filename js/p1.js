/**
 * Initializes p1 module.
 *  @function init
 * 
 * @param {Object} debug APEX debug module
 * 
 * @example
 * p1.init(apex.debug);
 **/
const init = (debug) => {
  debug.log("p1.init");
};

export default {
  init
};
