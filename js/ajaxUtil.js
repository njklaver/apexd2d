

/**
 * AJAX process utility Javascript functions.
 *
 * @module ajaxUtil
 */

const ajaxUtil = {};

/**
 * Initializes the utilities
 * @function init
 * 
 * @param {Object} debug   APEX Debug module
 * @param {Object} message APEX message module
 * @param {Object} lang    APEX lang module
 * @param {Object} region  APEX region module
 * @param {Object} util    Misc. utilities module
 * 
 * @returns {Object} AJAX process utilities module
 * 
 * @example
 * ajaxUtil = ajaxUtil.init(apex.debug, apex.message, apex.lang, apex.region, util);
 **/
const init = (debug, message, lang, region, util) => {
   debug.log("ajaxUtil.init");

   /**
    * Gives version info.
    * @function info
    */
   ajaxUtil.info = () => {
      debug.info('ajaxUtil v1.0.0');
   };

   /**
   * Excecute AJAX process for the selected rows.
   * @function execIg
   * 
   * @param igId {string} - Static ID Iteractive grid
   * @param pk {string} - PK column name
   * @param process {string} - AJAX process
   * @param completeMsg {string} - Success message code
   */
   ajaxUtil.execIg = (igId, pk, process, completeMsg) => {
      let view = region(igId).call("getViews", "grid");
      let model = view.model;
      var selected = view.getSelectedRecords();
      var pkList = [];
      selected.forEach((_object, index) => {
         var rec = selected[index];
         pkList.push(model.getValue(rec, pk));
      });
      apex.server.process(process,
         {
            f01: pkList
         },
         {
            loadingIndicator: "#" + igId,
            loadingIndicatorPosition: "centered",
            dataType: "text"
         }).then((data) => {
            debug.log(data);
            try {
               let response = JSON.parse(data);
               debug.log(response);
               message.showPageSuccess(lang.getMessage(completeMsg));
               view.refresh();
            } catch (e) {
               util.fatalError();
            }
         }).catch((xhr) => {
            debug.error(xhr);
            util.fatalError();
         });
   };


   return ajaxUtil;
};

export default {
   init
}


