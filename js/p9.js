/**
 * Initializes p9 module
 * @function init
 * 
 * @param {string} debug APEX debug module
 * 
 * @example
 * p9.init(apex.debug);
 **/
 const init = (debug) => {
   debug.log("p9.init");
};
  
export default {
 init
};

