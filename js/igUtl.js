/**
 * IG utility Javascript functions.
 *
 * @module ig
 */

const ig = {};

/**
 * Initializes IG utilities
 * @function init
 * 
 * @param {Object} debug APEX Debug module
 * 
 * @returns {Object} IG utilities module
 * 
 * @example
 * ig = igUtl.init(apex.debug);
 **/
const init = (debug) => {
	/**
	 * Gives version info.
  	 * @function info
	 */
	ig.info = () => {
		debug.info('ig v1.0.0');
	};

	/**
	 * Returns the value for the column given in the selected row of an IG.
	 *
	 * @function getSelected
	 *
	 * @param {string} igId Static ID IG region.
	 * @param {string} col  Column to handle
	 *
	 * @returns {string} Value found or undefined when multiple or zero rows are selected.
	 */
	ig.getSelected = (igId, col)  => {
		let val;
		const view = apex.region(igId).call('getViews', 'grid');
		const selected = view.getSelectedRecords();
		if (selected.length === 1) {
			val = view.model.getValue(selected[0], col);
		}

		return val;
	};

	/**
	 * Sets the row defined by the PK value given selected.
	 *
	 * @function setSelected
	 *
	 * @param {string} igId  Static ID IG region.
	 * @param {string} recId Row PK value to handle.
	 */
	ig.setSelected = (igId, recId) => {
		const view = apex.region(igId).call('getViews', 'grid');
		const rec = view.model.getRecord(recId);
		if (rec) {
			view.setSelectedRecords([rec], true);
		}
	};

	return ig;
};

export default {
	init
}
