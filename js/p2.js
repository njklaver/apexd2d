/**
 * Initializes p2 module.
 * @function init
 * 
 * @param {Object} debug APEX debug module
 * 
 * @example
 * init(apex.debug);
 **/
const init = (debug) => {
  debug.log("p2.init");
};

export default {
  init
};
