import p1$module from "./js/p1";
import p2$module from "./js/p2";
import p5$module from "./js/p5";
import p7$module from "./js/p7";
import p9$module from "./js/p9";
import p13$module from "./js/p13";
import igUtl$module from "./js/igUtl";
import igAction$module from "./js/igAction";
import util$module from "./js/util";
import ajaxUtil$module from "./js/ajaxUtil"

var ig;
var tt;
var igAction;
var dvw;
var util;
var ajaxUtil;


/*
* Initializes JS for the application.
*/
apex.jQuery(document).ready(() => {
  const pageId = Number(document.getElementById("pFlowStepId").value);

  switch (pageId) {
    case 1:
      p1$module.init(apex.debug);
      break;
    case 2:
      ig = igUtl$module.init(apex.debug);
      p2$module.init(apex.debug);
      break;
    case 5:
      ig = igUtl$module.init(apex.debug);
      p5$module.init(apex.debug);
      break;
    case 7:
      util = util$module.init(apex.debug, apex.message, apex.lang);
      ajaxUtil = ajaxUtil$module.init(apex.debug, apex.message, apex.lang, apex.region, util);
      ig = igUtl$module.init(apex.debug);
      tt = p7$module.init(apex.lang, apex.message, apex.region, apex.debug, ajaxUtil);
      igAction = igAction$module.init(apex.jQuery, apex.lang, apex.region, apex.debug, tt.action);
      break;
    case 9:
      ig = igUtl$module.init(apex.debug);
      p9$module.init(apex.debug);
      break;
    case 13:
      util = util$module.init(apex.debug, apex.message, apex.lang);
      ajaxUtil = ajaxUtil$module.init(apex.debug, apex.message, apex.lang, apex.region, util);
      ig = igUtl$module.init(apex.debug);
      dvw = p13$module.init(apex.lang, apex.message, apex.region, apex.debug, ajaxUtil);
      igAction = igAction$module.init(apex.jQuery, apex.lang, apex.region, apex.debug, dvw.action);
      break;
    default:
      apex.debug.log("default: " + pageId);
  }
});

export {
  ig,
  tt,
  dvw,
  igAction,
  util,
  ajaxUtil
}
